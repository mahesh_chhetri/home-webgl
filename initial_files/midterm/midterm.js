var canvas;
var gl;

var bufferNum1, bufferNum2, num1Vertices, num2Vertices;
var vPosition;
var transformationMatrix, transformationMatrixLoc;

window.onload = function init()
{
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    //
    //  Configure WebGL
    //
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    //  Load shaders and initialize attribute buffers
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Make the numbers
    num1Vertices = [
        -0.5,0.5,0.0,
        -0.5,-0.5,0.0,
        0.5,-0.5,0.0,
        0.5,0.5,0.0 
        
    ];

    num2Vertices = [
        vec2(  0.1,  -0.2, 05, -0.6 ),
        vec2(  0.3,  -0.2 ),
        vec2(  0.1,  0.2 ),
        vec2(  0.3,  0.2 ),
        vec2(  -0.3,  0.2 )
    ];
	
	//TODO: create and load geometry

    // Load the data into the GPU
    bufferNum1 = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferNum1 );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(num1Vertices), gl.STATIC_DRAW );

    // Load the data into the GPU
    bufferNum2 = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferNum2 );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(num2Vertices), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer
    vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    transformationMatrixLoc = gl.getUniformLocation( program, "transformationMatrix" );

	document.getElementById("inp_number").oninput = function(event) {
        //TODO: fill here to adjust number to display input value
    };
	
    document.getElementById("inp_objX").oninput = function(event) {
        //TODO: fill here to adjust translation according to slider value
    };
    document.getElementById("inp_objY").oninput = function(event) {
        //TODO: fill here to adjust translation according to slider value
    };
    document.getElementById("inp_obj_scaleX").oninput = function(event) {
        //TODO: fill here to adjust scale according to slider value
    };
    document.getElementById("inp_obj_scaleY").oninput = function(event) {
        //TODO: fill here to adjust scale according to slider value
    };
    document.getElementById("inp_rotation").oninput = function(event) {
        //TODO: fill here to adjust rotation according to slider value
    };
    document.getElementById("redSlider").oninput = function(event) {
        //TODO: fill here to adjust color according to slider value
    };
    document.getElementById("greenSlider").oninput = function(event) {
        //TODO: fill here to adjust color according to slider value
    };
    document.getElementById("blueSlider").oninput = function(event) {
        //TODO: fill here to adjust color according to slider value
    };

    render();

};


function render() {

    gl.clear( gl.COLOR_BUFFER_BIT );

	//TODO: send color to shader
	//TODO: calculate and send transformation matrix
	//TODO: draw digits
	
    transformationMatrix = mat4();
    gl.uniformMatrix4fv( transformationMatrixLoc, false, flatten(transformationMatrix) );

    gl.bindBuffer( gl.ARRAY_BUFFER, bufferNum1 );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.drawArrays( gl.TRIANGLE_STRIP, 0, 3 );
	
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferNum2 );
    gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
    gl.drawArrays( gl.TRIANGLE_STRIP, 0, 4 );

    window.requestAnimFrame(render);
}
